#!/bin/bash
# vim: ai:ts=8:sw=8:noet
# This script automates creation of snapshots under gitlab-ops project

set -eufo pipefail
IFS=$'\t\n'

output_date() {
	date "+%Y.%m.%d-%H:%M:%S %Z"
}

log() {
	>&2 echo "$(output_date) $*"
}

filter_out_patroni_nodes() {
	echo "name !~ patroni"
}

disk_list_filter() {
	echo "labels.do_snapshots=true $(filter_out_patroni_nodes)"
}

### Command requirements
command -v gcloud >/dev/null 2>/dev/null || {
	log 'Please install gcloud utility'
	exit 1
}

GCE_ZONE=${GCE_ZONE:-us-east1-c}
GCE_PROJECT=${GCE_PROJECT:-gitlab-ops}

CI_JOB_ID=${CI_JOB_ID:-notonCI}

# description
DESCRIPTION="snapshot created by CI job '${CI_JOB_ID}' on $(date)"

# snapshots before this date will be deleted
CLEANUP_DATE="$(date --date="14 days ago" +%Y-%m-%d)"

# Set zone in config if not set
gcloud config get-value compute/zone >/dev/null || \
	gcloud config set compute/zone "${GCE_ZONE}"
DISK_FILTER=$(disk_list_filter)
log "Querying disks to snapshot filter: ${DISK_FILTER} ..."
declare -a TO_SNAPSHOT
# TODO: check page size
readarray -t TO_SNAPSHOT < <(gcloud compute disks \
	list \
	--filter="${DISK_FILTER}" \
	--uri)

# NOTE: async is not only good, it also removes the need for compute.zoneOperations.get permission :)
log "Creating snapshots ..."
gcloud compute disks snapshot "${TO_SNAPSHOT[@]}" \
	--description="${DESCRIPTION}" \
	--async \
	--quiet

log "Querying snapshots to delete (older than ${CLEANUP_DATE}) ..."
declare -a TO_DELETE
readarray -t TO_DELETE < <(gcloud compute snapshots list \
	--filter="creationTimestamp<'${CLEANUP_DATE}'" \
	--uri)
log "${#TO_DELETE[@]} snapshots found."

if [[ ${#TO_DELETE[@]} -gt 0 ]]; then
	log "Cleaning up ${#TO_DELETE[@]} old snapshots ..."
	gcloud compute snapshots delete "${TO_DELETE[@]}" \
		--quiet >/dev/null 2>/dev/null || true
fi

log "Querying snapshots to delete (of size 0 bytes) ..."
declare -a EMPTY
readarray -t EMPTY < <(gcloud compute snapshots list \
	--filter="storageBytes=0 AND status=('READY')" \
	--uri)
log "${#EMPTY[@]} snapshots found."

if [[ "${#EMPTY[@]}" -gt 0 ]]; then
	log "Cleaning up ${#EMPTY[@]} empty snapshots ..."
	gcloud compute snapshots delete "${EMPTY[@]}" \
		--quiet >/dev/null 2>/dev/null || true
	log "Cleanup of empty snapshots completed"
fi

# let's have some fun now
log "Listing snapshots ..."
export TERM=xterm
apk add gnuplot
gcloud compute snapshots list \
	--format='json(creationTimestamp.date(format="%Y-%m-%d",tz=UTC),storageBytes)' \
	--filter="storageBytes>0 AND status=('READY')" | \
	jq -r -s '.[] | map({d:.creationTimestamp, s:.storageBytes|tonumber, c: 1}) | group_by(.d) | map({d:.[0].d, s:(map(.s)|add/1024/1024/1024|floor|tostring), c:map(.c)|add|tostring}) | .[] | join(",")' > /tmp/data
echo "Will now visualise the following data:"
cat /tmp/data
gnuplot -p -e "$(cat <<-EOF
	set autoscale;
	set boxwidth 0.01 absolute;
	set datafile sep ',';
	set logscale y2;
	set style fill solid noborder;
	set terminal dumb size 100,30 aspect 1 enhanced;
	set timefmt '%Y-%m-%d';
	set xdata time;
	set xtics 2*24*60*60;
	set y2tics;
	plot '/tmp/data' using 1:3 axes x1y1 title 'number of snapshots per day' with boxes, \
	     '/tmp/data' using (timecolumn(1)+6*60*60):2 axes x1y2 title 'snapshot size per day, GBytes (logscale)' with boxes
EOF
)" | \
	sed $'s/#/\e[35m#\e[0m/g;s/*/\e[36m*\e[0m/g' # pretty sure that's write-only code :)

log "Total snapshot quota usage: $(gcloud compute snapshots list --format="[table,no-heading](name)" | wc -l)/10k"
